(function( $ ) {
/**
 * START - ONLOAD - JS
 * slider(): Make slider
 */
/* ----------------------------------------------- */
/* ------------- FrontEnd Functions -------------- */
/* ----------------------------------------------- */
function slider() {
    $("#main-slider").owlCarousel({
        navigation      : true, // Show next and prev buttons
        slideSpeed      : 300,
        paginationSpeed : 400,
        singleItem      : true,
        pagination      : false
    });
}

/* ----------------------------------------------- */
/* ----------------------------------------------- */
/* OnLoad Page */ 
$(document).ready(function($){
    slider();
});
/* OnLoad Window */
var init = function () {

};
window.onload = init;

})(jQuery);
